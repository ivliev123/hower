#include "stm32f1xx_hal.h"
#include "defines.h"
#include "setup.h"
#include "config.h"
#include "stdio.h"
#include "string.h"

UART_HandleTypeDef huart2;

#ifdef DEBUG_SERIAL_USART3
#define UART_DMA_CHANNEL DMA1_Channel2
#endif

#ifdef DEBUG_SERIAL_USART2
#define UART_DMA_CHANNEL DMA1_Channel7
#endif


volatile uint8_t uart_buf[100];
volatile int ch_buf[16];
volatile int32_t ch_buf_int[20];
//volatile char char_buf[300];

void setScopeChannel(uint8_t ch, int16_t val) {
  ch_buf[ch] = val;
}

void setScopeChannelInt(uint8_t ch, int val) {
  ch_buf_int[ch] = val;
}

void consoleScope() {
  #if defined DEBUG_SERIAL_SERVOTERM && (defined DEBUG_SERIAL_USART2 || defined DEBUG_SERIAL_USART3)
//	ch_buf_int[0]=11;
//	ch_buf_int[1]=13;
    uart_buf[0] = 0xff;
    uart_buf[1] = 0xfe;
//    int x =-23;
//   memcpy((void *)&uart_buf[2],(void *)&x,4);

//    memcpy((void *)&uart_buf[1],0xff,1);
//    memcpy((void *)&uart_buf[2],0xfe,1);
//   int addr =2;
   memcpy((void *)&uart_buf[2],(void *)&ch_buf[2],4);
   memcpy((void *)&uart_buf[6],(void *)&ch_buf[3],4);
   memcpy((void *)&uart_buf[10],(void *)&ch_buf[7],4);
   memcpy((void *)&uart_buf[14],(void *)&ch_buf[8],4);
   memcpy((void *)&uart_buf[18],(void *)&ch_buf[9],4);
   memcpy((void *)&uart_buf[22],(void *)&ch_buf[0],4);
   memcpy((void *)&uart_buf[26],(void *)&ch_buf[1],4);

   memcpy((void *)&uart_buf[30],(void *)&ch_buf[10],4);
   memcpy((void *)&uart_buf[34],(void *)&ch_buf[11],4);
   memcpy((void *)&uart_buf[38],(void *)&ch_buf[12],4);
   memcpy((void *)&uart_buf[42],(void *)&ch_buf[13],4);
   memcpy((void *)&uart_buf[46],(void *)&ch_buf[14],4);
   memcpy((void *)&uart_buf[50],(void *)&ch_buf[15],4);

   memcpy((void *)&uart_buf[54],(void *)&ch_buf[5],4);
   memcpy((void *)&uart_buf[58],(void *)&ch_buf[6],4);
   uart_buf[62] = 0x00;
   uart_buf[66] = 0x00;

//    int addr =1;
//    uart_buf[2] = ch_buf_int[2] & 0xff;
//    uart_buf[3] = (ch_buf_int[2] >> 8) & 0xff;
//    uart_buf[4] = (ch_buf_int[2] >> 16) & 0xff;
//    uart_buf[5] = (ch_buf_int[2] >> 24) & 0xff;
//
//    uart_buf[6] = ch_buf_int[3] & 0xff;
//    uart_buf[7] = (ch_buf_int[3] >> 8) & 0xff;
//    uart_buf[8] = (ch_buf_int[3] >> 16) & 0xff;
//    uart_buf[a9] = (ch_buf_int[3] >> 24) & 0xff;
//    uart_buf[1] = CLAMP(ch_buf[2], 0, 10);
//    uart_buf[2] = CLAMP(ch_buf[3], 0, 255);
//    uart_buf[3] = CLAMP(ch_buf[7], 0, 255);
//    uart_buf[4] = CLAMP(ch_buf[8], 0, 255);
//    uart_buf[5] = CLAMP(ch_buf[9], 0, 255);
//    uart_buf[6] = CLAMP(ch_buf[0], 0, 255);
//    uart_buf[7] = CLAMP(ch_buf[1], 0, 255);
//    uart_buf[8] = CLAMP(ch_buf[7], 0, 255);
//    uart_buf[30] = '\n';

    if(UART_DMA_CHANNEL->CNDTR == 0) {
      UART_DMA_CHANNEL->CCR &= ~DMA_CCR_EN;
      UART_DMA_CHANNEL->CNDTR = 70;
      UART_DMA_CHANNEL->CMAR  = (uint32_t)uart_buf;
      UART_DMA_CHANNEL->CCR |= DMA_CCR_EN;
    }
  #endif

  #if defined DEBUG_SERIAL_ASCII && (defined DEBUG_SERIAL_USART2 || defined DEBUG_SERIAL_USART3 )
    memset(uart_buf, 0, sizeof(uart_buf));
//    sprintf(uart_buf, "1:%i 2:%i 3:%i 4:%i 5:%i 6:%i 7:%i 8:%i\r\n", ch_buf[0], ch_buf[1], ch_buf[2], ch_buf[3], ch_buf[4], ch_buf[5], ch_buf[6], ch_buf[7]);
    sprintf(uart_buf, "%i %i %i %i %i %i %i *\r\n", ch_buf[2], ch_buf[3], ch_buf[7], ch_buf[8], ch_buf[9], ch_buf[0],  ch_buf[1]);

    if(UART_DMA_CHANNEL->CNDTR == 0) {
      UART_DMA_CHANNEL->CCR &= ~DMA_CCR_EN;
      UART_DMA_CHANNEL->CNDTR = strlen(uart_buf);
      UART_DMA_CHANNEL->CMAR  = (uint32_t)uart_buf;
      UART_DMA_CHANNEL->CCR |= DMA_CCR_EN;
    }
  #endif
}

void consoleLog(char *message)
{
    HAL_UART_Transmit_DMA(&huart2, (uint8_t *)message, strlen(message));
}
