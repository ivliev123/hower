/*
* This file is part of the hoverboard-firmware-hack project.
*
* Copyright (C) 2017-2018 Rene Hopf <renehopf@mac.com>
* Copyright (C) 2017-2018 Nico Stute <crinq@crinq.de>
* Copyright (C) 2017-2018 Niklas Fauth <niklas.fauth@kit.fail>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stm32f1xx_hal.h"
#include "defines.h"
#include "setup.h"
#include "config.h"
#include "math.h"
#include "float.h"
//#include "hd44780.h"

void SystemClock_Config(void);

extern TIM_HandleTypeDef htim_left;
extern TIM_HandleTypeDef htim_right;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern volatile adc_buf_t adc_buffer;
//LCD_PCF8574_HandleTypeDef lcd;
extern I2C_HandleTypeDef hi2c2;
extern UART_HandleTypeDef huart2;

int cmd1;  // normalized input values. -1000 to 1000
int cmd2;
int cmd3;

extern volatile uint32_t Millis_R;
extern volatile uint32_t Millis_L;

extern volatile uint32_t d_Millis_R;
extern volatile uint32_t d_Millis_L;

extern volatile uint32_t Millis_R_last;
extern volatile uint32_t Millis_L_last;

extern volatile uint32_t Millis_R_now;
extern volatile uint32_t Millis_L_now;



//typedef struct{
//   int byte1;
//   int byte2;
//   int steer;
//   int speed;
////   int dir1;
////   int dir2;
//
//   //uint32_t crc;
//} Serialcommand;

typedef struct{
   int data1;
   int data2;
   int data3;
   int data4;
   int data5;
   int data6;
   int data7;
   int data8;
   //uint32_t crc;
} Serialcommand;


volatile Serialcommand command;

uint8_t button1, button2;

int steer; // global variable for steering. -1000 to 1000
int speed; // global variable for speed. -1000 to 1000

extern volatile int pwml;  // global variable for pwm left. -1000 to 1000
extern volatile int pwmr;  // global variable for pwm right. -1000 to 1000
extern volatile int weakl; // global variable for field weakening left. -1000 to 1000
extern volatile int weakr; // global variable for field weakening right. -1000 to 1000

extern volatile int d_posr;
extern volatile int d_posl;

float d_r_m=0.000;
float d_l_m=0.000;
float d_theta=0.000;

float d_s_m=0.000;

float x=0.000;
float y=0.000;
float theta=0.000;

float x_now=0.000;
float y_now=0.000;
float theta_now=0.000;

float x_last=0.000;
float y_last=0.000;
float theta_last=0.000;

float d_x=0.000;
float d_y=0.000;
float d_theta_s=0.000;

float x_now_s=0.000;
float y_now_s=0.000;

float x_last_s=0.000;
float y_last_s=0.000;

float d_x_s=0.000;
float d_y_s=0.000;


float R=0.000;
float x_r=0.000;
float y_r=0.000;

int x_int=0;
int y_int=0;
int theta_int=0;

extern volatile int posl_summ;
extern volatile int posr_summ;

volatile int posl_summ_now=0;
volatile int posr_summ_now=0;

volatile int posl_summ_last=0;
volatile int posr_summ_last=0;

volatile int d_posr_summ=0;
volatile int d_posl_summ=0;

int posl_summ_now_w=0;
int posr_summ_now_w=0;

int d_posl_summ_w=0;
int d_posr_summ_w=0;

int posl_summ_last_w=0;
int posr_summ_last_w=0;


float vx=0.000;
float w=0.000;

int vx_int=0;
int w_int=0;


float S=0.000;

int S_int=0;

float WL_to=0;
float WR_to=0;

float vx_to=0.00;
float w_to=0.00;


extern float w_l;
extern float w_r;

int w_l_int=0;
int w_r_int=0;

int time_Millis=0;

int vx_to_int=0;
int w_to_int=0;



float error_vx=0.00;
float total_error_vx =0.00;
float  d_error_vx=0.00;

float Kp_vx=0.4;
float Ki_vx=0.2;
float Kd_vx=-0.1;

float pid_vx= 0.00;

float last_error_vx=0.00;

float vx_to_new=0.00;



float error_w=0.00;
float total_error_w= 0.00;
float d_error_w=0.00;

float Kp_w=0.8;
float Ki_w=0.3;
float Kd_w=-0.1;

float pid_w= 0.00;

float last_error_w=0.00;

float w_to_new=0.00;



int error_vx_int=0;
int pid_vx_int=0;

float w_l_to_new=0;
float w_r_to_new=0;


int flag_vx=1;

volatile int dir1_ser;
volatile int dir2_ser;

volatile int byte_uart_1;
volatile int byte_uart_2;


int P_w=0;
int I_w=0;
int D_w=0;

int P_v=0;
int I_v=0;
int D_v=0;

int stop_data= 0;
int test_uart=0;
int odom_to_0=0;
int test_uart_last=0;

float a_vx=0.0;
float vx_now=0.0;
float vx_last=0.0;

float a_w=0.0;
float w_now=0.0;
float w_last=0.0;

int a_vx_int=0;
int a_w_int=0;

float L_sepr = 0.455;
float R_wheel = 0.0825;
float pi=3.14159265359;

uint32_t Uart_milles=0;
uint32_t Uart_milles_last=0;


extern uint8_t buzzerFreq;    // global variable for the buzzer pitch. can be 1, 2, 3, 4, 5, 6, 7...
extern uint8_t buzzerPattern; // global variable for the buzzer pattern. can be 1, 2, 3, 4, 5, 6, 7...

extern uint8_t enable; // global variable for motor enable

extern volatile uint32_t timeout; // global variable for timeout
extern float batteryVoltage; // global variable for battery voltage

uint32_t inactivity_timeout_counter;

extern uint8_t nunchuck_data[6];
#ifdef CONTROL_PPM
extern volatile uint16_t ppm_captured_value[PPM_NUM_CHANNELS+1];
#endif

int milli_vel_error_sum = 0;


void poweroff() {
    if (abs(speed) < 20) {
        buzzerPattern = 0;
        enable = 0;
        for (int i = 0; i < 8; i++) {
            // buzzerFreq = i;
            HAL_Delay(100);
        }
        HAL_GPIO_WritePin(OFF_PORT, OFF_PIN, 0);
        while(1) {}
    }
}


int main(void) {
  HAL_Init();
  __HAL_RCC_AFIO_CLK_ENABLE();
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
  /* System interrupt init*/
  /* MemoryManagement_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);
  /* BusFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(BusFault_IRQn, 0, 0);
  /* UsageFault_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(UsageFault_IRQn, 0, 0);
  /* SVCall_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SVCall_IRQn, 0, 0);
  /* DebugMonitor_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DebugMonitor_IRQn, 0, 0);
  /* PendSV_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(PendSV_IRQn, 0, 0);
  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

  SystemClock_Config();

  __HAL_RCC_DMA1_CLK_DISABLE();
  MX_GPIO_Init();
  MX_TIM_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();

  #if defined(DEBUG_SERIAL_USART2) || defined(DEBUG_SERIAL_USART3)
    UART_Init();
  #endif

  HAL_GPIO_WritePin(OFF_PORT, OFF_PIN, 1);

  HAL_ADC_Start(&hadc1);
  HAL_ADC_Start(&hadc2);

  for (int i = 8; i >= 0; i--) {
    // buzzerFreq = i;
    HAL_Delay(100);
  }
//  buzzerFreq = 0;

  HAL_GPIO_WritePin(LED_PORT, LED_PIN, 1);

  int lastSpeedL = 0, lastSpeedR = 0;
  int speedL = 0, speedR = 0;
  int speedL2 = 0, speedR2 = 0;
  float direction = 1;

  #ifdef CONTROL_PPM
    PPM_Init();
  #endif

  #ifdef CONTROL_NUNCHUCK
    I2C_Init();
    Nunchuck_Init();
  #endif

  #ifdef CONTROL_SERIAL_USART2
    UART_Control_Init();
    HAL_UART_Receive_DMA(&huart2, (int *)&command, 32);
  #endif

  #ifdef DEBUG_I2C_LCD
    I2C_Init();
    HAL_Delay(50);
    lcd.pcf8574.PCF_I2C_ADDRESS = 0x27;
      lcd.pcf8574.PCF_I2C_TIMEOUT = 5;
      lcd.pcf8574.i2c = hi2c2;
      lcd.NUMBER_OF_LINES = NUMBER_OF_LINES_2;
      lcd.type = TYPE0;

      if(LCD_Init(&lcd)!=LCD_OK){
          // error occured
          //TODO while(1);
      }

    LCD_ClearDisplay(&lcd);
    HAL_Delay(5);
    LCD_SetLocation(&lcd, 0, 0);
    LCD_WriteString(&lcd, "Hover V2.0");
    LCD_SetLocation(&lcd, 0, 1);
    LCD_WriteString(&lcd, "Initializing...");
  #endif

  float board_temp_adc_filtered = (float)adc_buffer.temp;
  float board_temp_deg_c;

  enable = 1;  // enable motors

  while(1) {
    HAL_Delay(DELAY_IN_MAIN_LOOP); //delay in ms

    #ifdef CONTROL_NUNCHUCK
      Nunchuck_Read();
      cmd1 = CLAMP((nunchuck_data[0] - 127) * 8, -1000, 1000); // x - axis. Nunchuck joystick readings range 30 - 230
      cmd2 = CLAMP((nunchuck_data[1] - 128) * 8, -1000, 1000); // y - axis

      button1 = (uint8_t)nunchuck_data[5] & 1;
      button2 = (uint8_t)(nunchuck_data[5] >> 1) & 1;
    #endif

    #ifdef CONTROL_PPM
      cmd1 = CLAMP((ppm_captured_value[0] - 500) * 2, -1000, 1000);
      cmd2 = CLAMP((ppm_captured_value[1] - 500) * 2, -1000, 1000);
      button1 = ppm_captured_value[5] > 500;
      float scale = ppm_captured_value[2] / 1000.0f;
    #endif

    #ifdef CONTROL_ADC
      // ADC values range: 0-4095, see ADC-calibration in config.h
      // cmd1 = CLAMP(adc_buffer.l_tx2 - ADC1_MIN, 0, ADC1_MAX) / (ADC1_MAX / 1000.0f);  // ADC1
      // cmd2 = CLAMP(adc_buffer.l_rx2 - ADC2_MIN, 0, ADC2_MAX) / (ADC2_MAX / 1000.0f);  // ADC2

      // use ADCs as button inputs:
      button1 = (uint8_t)(adc_buffer.l_tx2 > 2000);  // ADC1
      button2 = (uint8_t)(adc_buffer.l_rx2 > 2000);  // ADC2

      timeout = 0;
    #endif

    #ifdef CONTROL_SERIAL_USART2
      byte_uart_1=command.data1;
	  byte_uart_2=command.data2;

	  if (command.data1==254 && command.data2==255){

      vx_to_int=command.data3;//command.steer;
      w_to_int=command.data4;//command.speed;
      stop_data=command.data5;
      test_uart=command.data6;
//      odom_to_0=command.data7;

      Millis_L=0;
	  }

	  if (command.data1==253 && command.data2==255){

      P_v=command.data3/100;
      I_v=command.data4/100;
      D_v=command.data5/100;


      P_w=command.data6/100;
      I_w=command.data7/100;
      D_w=command.data8/100;

	  }

	  if (command.data1==250 && command.data2==255){

      x=command.data3/100;
      y=command.data4/100;
      theta=command.data5/100;



	  }


	  else{

	  }



//	    Millis_L++;
//	    if (Millis_L>10){
//			command.data1=0;
//			command.data2=0;
//			command.data3=0;
//			command.data4=0;
//			command.data5=0;
//			command.data6=0;
//			command.data7=0;
//			command.data8=0;
//			speedL=0;
//			speedR=0;
//			vx_to_int=0;
//			w_to_int=0;
//	    }


      timeout = 0;
    #endif





      Uart_milles =HAL_GetTick();

      if (test_uart!=test_uart_last){
    	  Uart_milles_last=Uart_milles;
      }
//      Uart_milles_last=Uart_milles;
      if (Uart_milles-Uart_milles_last>300){
  		vx_to_int=0;
  		w_to_int=0;
      }
      test_uart_last=test_uart;



	if (stop_data==0){
		vx_to_int=0;
		w_to_int=0;
	}


    vx_to=vx_to_int/100.00;
    w_to=w_to_int/100.00;

    Millis_R=HAL_GetTick();


    Millis_R_now=Millis_R;
    x_now=x;
    y_now=y;
    theta_now=theta;
    int d_t =100;

    if (Millis_R_now-Millis_R_last>=d_t){
    	Millis_R_last=Millis_R_now;

		posl_summ_now_w=posl_summ;
		posr_summ_now_w=posr_summ;

		d_posl_summ_w=posl_summ_now_w-posl_summ_last_w;
		d_posl_summ_w=-1*d_posl_summ_w;
		d_posr_summ_w=posr_summ_now_w-posr_summ_last_w;
//		d_posr_summ_w=-1*d_posr_summ_w;

		posl_summ_last_w=posl_summ_now_w;
		posr_summ_last_w=posr_summ_now_w;

//		if (d_posr_summ_w>=0 && d_posl_summ_w<=0){
//			d_posl_summ_w=abs(d_posl_summ_w);
//			d_posr_summ_w=abs(d_posr_summ_w);
//			}
//		else if (d_posr_summ_w<=0 && d_posl_summ_w>=0){
//			d_posl_summ_w=-abs(d_posl_summ_w);
//			d_posr_summ_w=-abs(d_posr_summ_w);
//			}
//
//		else if (d_posr_summ_w>=0 && d_posl_summ_w>=0){
//			d_posl_summ_w=-abs(d_posl_summ_w);
//			d_posr_summ_w=abs(d_posr_summ_w);
//		}
//		else if (d_posr_summ_w<=0 && d_posl_summ_w<=0){
//			d_posl_summ_w=abs(d_posl_summ_w);
//			d_posr_summ_w=-abs(d_posr_summ_w);
//			}


    	d_x=x_now-x_last;
    	d_y=y_now-y_last;
    	d_theta_s=theta_now-theta_last;

    	float d_ttt=1000/d_t;

    	w_l=d_posl_summ_w*d_ttt*2*pi/90;
    	w_r=d_posr_summ_w*d_ttt*2*pi/90;



    	vx=(w_l*R_wheel+w_r*R_wheel)/2.0;
    	w=(w_l*R_wheel - w_r*R_wheel)/L_sepr;

//ускорение
    	vx_now=vx;
    	a_vx=(vx_now-vx_last)*d_ttt;
    	vx_last=vx_now;

    	w_now=w;
    	a_w=(w_now-w_last)*d_ttt;
    	w_last=w_now;

    	a_vx_int=a_vx*1000;
    	a_w_int=a_w*1000;

//    	vx=(d_x*d_ttt/(cos(theta))+d_y*d_ttt/(sin(theta)))/2.0;         //REAL
//    	w=d_theta_s*d_ttt;											  //REAL

    	vx_int=vx*1000;
    	w_int=w*1000;


    	//перед тем как значения подавать на пид их нижно прфильтровать P.S так как хтел Данил


//    	if (vx>0 && vx_to<=0){
//    		vx_to=0;
//    	}
//
//    	if (vx<0 && vx_to>=0){
//    	    vx_to=0;
//    	}
//
//    	if (w>0 && w_to<=0){
//    		w_to=0;
//    	}
//    	if (w<0 && w_to>=0){
//    		w_to=0;
//    	}


//      ПИД на поступательную скорость
        error_vx=vx_to-vx;
        total_error_vx = total_error_vx +  error_vx;
        if (total_error_vx>=5){
        	total_error_vx=5;
        }
        if (total_error_vx<=-5){
        	total_error_vx=-5;
        }
        d_error_vx=error_vx-last_error_vx;


        pid_vx= Kp_vx*error_vx +  Ki_vx*total_error_vx + Kd_vx*d_error_vx; //

        P_v=Kp_vx*error_vx *100;
        I_v=Ki_vx*total_error_vx*100;
		D_v=Kd_vx*d_error_vx*100;


//      ПИД на вращательную скорость
        error_w=w_to-w;
        total_error_w += error_w;
        if (total_error_w>=5){
        	total_error_w=5;
        }
        if (total_error_w<=-5){
        	total_error_w=-5;
        }
        d_error_w=error_w-last_error_w;


        pid_w= Kp_w*error_w + Ki_w*total_error_w  + Kd_w*d_error_w;//;

        P_w=Kp_w*error_w*100;
        I_w=Ki_w*total_error_w*100;
		D_w=Kd_w*d_error_w*100;

//        error_vx_int=d_error_w*100;

        vx_to_new=pid_vx;

        w_to_new=pid_w;


        if (vx_to_new>=1.0){
        	vx_to_new=1.0;
        }
        if (vx_to_new<=-1.0){
        	vx_to_new=-1.0;
        }

        if (w_to_new>=2.0){
        	w_to_new=2.0;
        }
        if (w_to_new<=-2.0){
        	w_to_new=-2.0;
        }
		w_l_to_new=(vx_to_new+(L_sepr*w_to_new)/2.0)/R_wheel;
		w_r_to_new=(vx_to_new-(L_sepr*w_to_new)/2.0)/R_wheel;

//        w_int=w*100;

        int L =w_l_to_new;
        int R =w_r_to_new;
        speedL2=L*17;
        speedR2=R*17;
//        if (speedL2<=1){
//        	speedL2=0;
//        }
//        if (speedR2<=1){
//        	speedR2=0;
//        }

        last_error_vx=error_vx;
        last_error_w=error_w;


    	x_last=x;
		y_last=y;
		theta_last=theta;

		w_l_int=w_l*1000;
		w_r_int=w_r*1000;

//		Millis_R_last=Millis_R_now;

    }





	posl_summ_now=posl_summ;
	posr_summ_now=posr_summ;


	d_posl_summ=posl_summ_now-posl_summ_last;
	d_posl_summ=-1*d_posl_summ;
	d_posr_summ=posr_summ_now-posr_summ_last;

	posl_summ_last=posl_summ_now;
	posr_summ_last=posr_summ_now;






	d_r_m=d_posr_summ*R_wheel*2*pi/90;
	d_l_m=d_posl_summ*R_wheel*2*pi/90;


	if (d_r_m==d_l_m){
		d_theta=(d_l_m - d_r_m)/L_sepr;
		d_s_m=(d_r_m + d_l_m)/2;



		x+= d_s_m*cos(theta);
		y+= d_s_m*sin(theta);
		theta+=d_theta;

//	      if (odom_to_0==1){
//	    	  x=0;
//	    	  y=0;
//	    	  theta=0;
//	      }

		x_int=x*100;
		y_int=y*100;
		theta_int=theta*100;

	}
	else{
		R=L_sepr*(d_l_m+d_r_m)/(2*(d_l_m-d_r_m));
		x_r=x-R*sin(theta);
		y_r=y+R*cos(theta);
		d_theta=(d_l_m - d_r_m)/L_sepr;

		x=x_r+R*sin(theta+d_theta);
		y=y_r-R*cos(theta+d_theta);
		theta+=d_theta;

//	      if (odom_to_0==1){
//	    	  x=0;
//	    	  y=0;
//	    	  theta=0;
//	      }


		x_int=x*100;
		y_int=y*100;
		theta_int=theta*100;

	}

//    if (odom_to_0==1){
//  	  x=0;
//  	  y=0;
//  	  theta=0;
//    }

	 x_int=x*100;
	 y_int=y*100;
	 theta_int=theta*100;



     speedL=-speedL2;
     speedR=-speedR2;

//     if (stop_data==0){
//    	 pwmr=0;
//    	 pwml=0;
//         speedL=0;
//         speedR=0;
//
//     }
//     else if (stop_data==1){
//         speedL=-speedL2;
//         speedR=-speedR2;
//     }
//    	speedR=50;
//    	speedL=50;



    #ifdef ADDITIONAL_CODE
      ADDITIONAL_CODE;
    #endif


    // ####### SET OUTPUTS #######
    if ((speedL < lastSpeedL + 50 && speedL > lastSpeedL - 50) && (speedR < lastSpeedR + 50 && speedR > lastSpeedR - 50) && timeout < TIMEOUT) {
    #ifdef INVERT_R_DIRECTION
      pwmr = speedR;
    #else
      pwmr = -speedR;
    #endif
    #ifdef INVERT_L_DIRECTION
      pwml = -speedL;
    #else
      pwml = speedL;
    #endif
    }

    lastSpeedL = speedL;
    lastSpeedR = speedR;



    if (inactivity_timeout_counter % 1 == 0) {
      // ####### CALC BOARD TEMPERATURE #######
      board_temp_adc_filtered = board_temp_adc_filtered * 0.99 + (float)adc_buffer.temp * 0.01;
      board_temp_deg_c = ((float)TEMP_CAL_HIGH_DEG_C - (float)TEMP_CAL_LOW_DEG_C) / ((float)TEMP_CAL_HIGH_ADC - (float)TEMP_CAL_LOW_ADC) * (board_temp_adc_filtered - (float)TEMP_CAL_LOW_ADC) + (float)TEMP_CAL_LOW_DEG_C;

      // ####### DEBUG SERIAL OUT #######
      #ifdef CONTROL_ADC
        setScopeChannel(0, (int)adc_buffer.l_tx2);  // 1: ADC1
        setScopeChannel(1, (int)adc_buffer.l_rx2);  // 2: ADC2
      #endif
        setScopeChannel(0, (int)a_vx_int);
        setScopeChannel(1, (int)a_w_int);
      setScopeChannel(2, (int)x_int);  // 3: output speed: 0-1000
      setScopeChannel(3, (int)y_int);  // 4: output speed: 0-1000
      setScopeChannel(4, (int)adc_buffer.batt1);  // 5: for battery voltage calibration
      setScopeChannel(5, (int)(batteryVoltage * 100.0f));  // 6: for verifying battery voltage calibration
      setScopeChannel(6, (int)board_temp_deg_c);  // 7: for board temperature calibration
      setScopeChannel(7, (int)theta_int);  // 8: for verifying board temperature calibration

      setScopeChannel(8, (int)vx_int);
      setScopeChannel(9, (int)w_int);

      setScopeChannel(10, (int)P_v);
      setScopeChannel(11, (int)I_v);
      setScopeChannel(12, (int)D_v);

      setScopeChannel(13, (int)P_w);
      setScopeChannel(14, (int)I_w);
      setScopeChannel(15, (int)D_w);

      consoleScope();
    }


    // ####### POWEROFF BY POWER-BUTTON #######
    if (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN) && weakr == 0 && weakl == 0) {
      enable = 0;
      while (HAL_GPIO_ReadPin(BUTTON_PORT, BUTTON_PIN)) {}
      poweroff();
    }


    // ####### BEEP AND EMERGENCY POWEROFF #######
    if ((TEMP_POWEROFF_ENABLE && board_temp_deg_c >= TEMP_POWEROFF && abs(speed) < 20) || (batteryVoltage < ((float)BAT_LOW_DEAD * (float)BAT_NUMBER_OF_CELLS) && abs(speed) < 20)) {  // poweroff before mainboard burns OR low bat 3
      poweroff();
    } else if (TEMP_WARNING_ENABLE && board_temp_deg_c >= TEMP_WARNING) {  // beep if mainboard gets hot
      buzzerFreq = 4;
      buzzerPattern = 1;
    } else if (batteryVoltage < ((float)BAT_LOW_LVL1 * (float)BAT_NUMBER_OF_CELLS) && batteryVoltage > ((float)BAT_LOW_LVL2 * (float)BAT_NUMBER_OF_CELLS) && BAT_LOW_LVL1_ENABLE) {  // low bat 1: slow beep
      buzzerFreq = 5;
      buzzerPattern = 42;
    } else if (batteryVoltage < ((float)BAT_LOW_LVL2 * (float)BAT_NUMBER_OF_CELLS) && batteryVoltage > ((float)BAT_LOW_DEAD * (float)BAT_NUMBER_OF_CELLS) && BAT_LOW_LVL2_ENABLE) {  // low bat 2: fast beep
      buzzerFreq = 5;
      buzzerPattern = 6;
    } else if (BEEPS_BACKWARD && speed < -50) {  // backward beep
      buzzerFreq = 5;
      buzzerPattern = 1;
    } else {  // do not beep
//      buzzerFreq = 0;
//      buzzerPattern = 0;
    }


    // ####### INACTIVITY TIMEOUT #######
    if (abs(speedL) >=0 || abs(speedR) >= 0) {
      inactivity_timeout_counter = 0;
    } else {
      inactivity_timeout_counter ++;
    }
    if (inactivity_timeout_counter > (INACTIVITY_TIMEOUT * 60 * 1000) / (DELAY_IN_MAIN_LOOP + 1)) {  // rest of main loop needs maybe 1ms
      poweroff();
    }
  }
}

/** System Clock Configuration
*/
void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLL_MUL16;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType      = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection    = RCC_ADCPCLK2_DIV8;  // 8 MHz
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

  /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}
