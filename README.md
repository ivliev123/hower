# Настройки для программатора ST Link 

[Инструкция по установке драйверов](https://github.com/texane/stlink#installation).



```
$ sudo apt-get install cmake
$ sudo apt-get install libusb-1.0.0
$ git clone https://github.com/texane/stlink.git
$ cd stlink
$ make release
$ cd build/Release; sudo make install
$ sudo ldconfig
```

## Использование ST link

Проверка бнаружения ST link:
```
$ st-info --probe
```
В ответ мы должны увидеть что-то вроде:
```
Found 1 stlink programmers
 serial: 563f7206513f52504832153f
openocd: "\x56\x3f\x72\x06\x51\x3f\x52\x50\x48\x32\x15\x3f"
  flash: 262144 (pagesize: 2048)
   sram: 65536
 chipid: 0x0414
  descr: F1 High-density device
```
## Подключение программатора к плате гироскутера 
Программатор требуется подключить к разьемам SWD Programming
![Label Tool](./pinout.png)


## Разблокировка платы гироскутера

Разблокировка с помощью openocd:
```
$ openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg -c init -c "reset halt" -c "stm32f1x unlock 0" -c "reset halt" -c "exit"
```
В ответ вы должны увидеть кучу сообщений INFO, а также:
```
stm32x unlocked.
```


Если не работает попробуйте следуйщие команды:
```
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg -c init -c "reset halt" -c "stm32f1x unlock 0"
```
```
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg -c init -c "reset halt" -c "mww 0x40022004 0x45670123" -c "mww 0x40022004 0xCDEF89AB" -c "mww 0x40022008 0x45670123" -c "mww 0x40022008 0xCDEF89AB" -c "mww 0x40022010 0x220" -c "mww 0x40022010 0x260" -c "sleep 100" -c "mww 0x40022010 0x230" -c "mwh 0x1ffff800 0x5AA5" -c "sleep 1000" -c "mww 0x40022010 0x2220" -c "sleep 100" -c "mdw 0x40022010" -c "mdw 0x4002201c" -c "mdw 0x1ffff800" -c targets -c "halt" -c "stm32f1x unlock 0"
```
```
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg -c init -c "reset halt" -c "mww 0x40022004 0x45670123" -c "mww 0x40022004 0xCDEF89AB" -c "mww 0x40022008 0x45670123" -c "mww 0x40022008 0xCDEF89AB" -c targets -c "halt" -c "stm32f1x unlock 0"
```




# Скачайте проект 
```
git clone https://gitlab.com/ivliev123/hower/
```




# Создание проекта под System Workbench Setup

## Создание проекта
1) Нажмите File > New > C Project
2) C Project
    1) Введите название проекта
    2) Выберите тип проекта: Executable > Ac6 STM32 MCU Project
    3) Выберите Toolchains: Ac6 STM32 MCU GCC
    4) Нажмите «Далее»
3) Выберите «Конфигурации»
    1) Нажмите «Next» 
4) Target Configuration
    1) Нажмите "Create a new custom board"
        1) Сохранить Определение новой платы
        2) Введите имя новой платы: STM32F103
        3) Выберите чип платы: STM32F1
        4) Выберите МСU: STM32F103RCTx
        5) Нажмите «ОК».
    2) Выберите плату, которое вы только что создали!
        1) Выбрать серию: STM32F1
        2) Выберите плату: STM32F103 
    3) Нажмите «Finish»
    
## Добавление файлов
1) Перетащите директорию «inc», «src» и «Drivers»  и поместите их в папку проекта в SystemWorkbench
2) Он предложит вам выбрать способ импорта файлов и папок: "Copy files and folders"
3) Будет предложено перезаписать? - перезаписать и для inc & src (вы перезаписываете пустые каталоги)
4) Замените загрузочный файл, перетащив файл startup\ _stm32f103xe.s 
    и удалите существующий файл startup_stm32.s
5) Включение каталога Drivers в сборке:
    1) Щелкните правой кнопкой мыши по каталогу
    2) Перейдите в раздел  Configurations > Exclude from Build.
    3) Deselect All
    4) нажмите «ОК»
6) Исключить с сборки drivers > cmsis directory
    1) Щелкните правой кнопкой мыши по каталогу
    2) Перейдите в раздел Конфигурации ресурсов> Исключить из сборки ...
    3) Select All
    4) нажмите «ОК»
7) Исключите некоторые из файлов hal в drivers > stm32f1xx_hal_driver > src:
    1) Select all the files
    2) Снимите выделение adc_ex, adc, cortex, dma, gpio_ex, gpio, iwdg, rcc_ex, rcc, tim_ex, tim, uart, hal
    2) Перейдите в раздел Configurations > Exclude from Build
    3) Select All
    4) нажмите «ОК»
    
## Конфигурация
1) Щелкните правой кнопкой мыши на папке проекта
2) Перейти к Properties
3) Перейти к  C/C++ General > Paths and Symbols
4)Includes
    1) Нажмите  Add ... 
    2) Выберите каталоги ниже с помощью файловой системы (и установите флажок «ДAdd to all configurations»)
      motor_controller/inc
        -motor_controller/drivers/stm32f1xx_hal_driver/inc
        -motor_controller/drivers/cmsis/device/st/stm32f1xx/inc
        -motor_controller/drivers/cmsis/inc
              
3) Symbols
    1) Нажмите «Add».
        1) Название: STM32F103xE; неважно
    4) Нажмите «ОК».
    


# Прошивка драйвера
```
st-flash write Debug/tet_usart23.bin 0x8000000
```


